﻿window.TestAngular = angular.module('TestAngular', ['ngRoute']); //.factory('entitymanagerprovider', [entitymanagerprovider]);;

// Add global "services" (like breeze and Q) 
TestAngular.value('breeze', window.breeze)
    .value('Q', window.Q);

//confifure runtime 
TestAngular.run(['$rootScope', 'entitymanagerprovider', '$q', 'breeze', function ($rootScope, entityManagerProvider, $q, breeze) {
    //var defer = $q.defer();
    //var defer = Q.defer();
    //entityManagerProvider.prepare().then(function () {
    //    defer.resolve();
    //});
    //return defer.promise;
    entityManagerProvider.whenReady.then(function () {
        entityManagerProvider.prepare();
    });
    
}]);

// Configure routes
TestAngular.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.
        when('/', { templateUrl: 'app/views/inventory/inventory.view.html', controller: 'InventoryCtrl' }).
        when('/about', { templateUrl: 'app/views/about/about.view.html', controller: 'AboutCtrl' }).
        otherwise({ redirectTo: '/' });
}]);

//#region Ng directives
TestAngular.directive('onFocus', function () {
    return {
        restrict: 'A',
        link: function (scope, elm, attrs) {
            elm.bind('focus', function () {
                scope.$apply(attrs.onFocus);
            });
        }
    };
})
    .directive('onBlur', function () {
        return {
            restrict: 'A',
            link: function (scope, elm, attrs) {
                elm.bind('blur', function () {
                    scope.$apply(attrs.onBlur);
                });
            }
        };
    })
    .directive('onEnter', function () {
        return function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {
                if (event.which === 13) {
                    scope.$apply(function () {
                        scope.$eval(attrs.onEnter);
                    });

                    event.preventDefault();
                }
            });
        };
    })
    .directive('selectedWhen', function () {
        return function (scope, elm, attrs) {
            scope.$watch(attrs.selectedWhen, function (shouldBeSelected) {
                if (shouldBeSelected) {
                    elm.select();
                }
            });
        };
    });
if (!Modernizr.input.placeholder) {
    // this browser does not support HTML5 placeholders
    // http://stackoverflow.com/questions/14777841/angularjs-inputplaceholder-directive-breaking-with-ng-model
    TestAngular.directive('placeholder', function () {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, element, attr, ctrl) {

                var value;

                var placeholder = function () {
                    element.val(attr.placeholder);
                };
                var unplaceholder = function () {
                    element.val('');
                };

                scope.$watch(attr.ngModel, function (val) {
                    value = val || '';
                });

                element.bind('focus', function () {
                    if (value == '') unplaceholder();
                });

                element.bind('blur', function () {
                    if (element.val() == '') placeholder();
                });

                ctrl.$formatters.unshift(function (val) {
                    if (!val) {
                        placeholder();
                        value = '';
                        return attr.placeholder;
                    }
                    return val;
                });
            }
        };
    });
}
//#endregion 