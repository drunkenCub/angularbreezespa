﻿TestAngular.factory('repository',[function () {

    var Repository = (function () {

        var repository = function (entityManagerProvider, entityTypeName, resourceName, fetchStrategy) {

            // Ensure resourceName is registered
            var entityType;
            if (entityTypeName) {
                entityType = getMetastore().getEntityType(entityTypeName);
                entityType.setProperties({ defaultResourceName: resourceName });

                getMetastore().setEntityTypeForResourceName(resourceName, entityTypeName);
            }

            this.withId = function (key) {
                if (!entityTypeName)
                    throw new Error("Repository must be created with an entity type specified");

                return manager().fetchEntityByKey(entityTypeName, key, true)
                    .then(function (data) {
                        if (!data.entity)
                            throw new Error("Entity not found!");
                        return data.entity;
                    });
            };

            this.find = function (predicate) {
                var query = breeze.EntityQuery
                    .from(resourceName)
                    .where(predicate);

                return executeQuery(query);
            };

            this.findAndOrder = function (predicate, orderPredicate) {
                var query = breeze.EntityQuery.from(resourceName).where(predicate).orderBy(orderPredicate);

                return executeQuery(query);
            };

            this.orderBy = function (orderPredicate) {
                var query = breeze.EntityQuery.from(resourceName).orderBy(orderPredicate);

                return executeQuery(query);
            };

            this.expandWithFindAndOrder = function (predicate, orderPredicate, expandPredicate) {
                var query = breeze.EntityQuery
                    .from(resourceName)
                    .expand(expandPredicate)
                    .where(predicate)
                    .orderBy(orderPredicate);

                return executeQuery(query);
            };

            this.expand = function (predicate) {
                var query = breeze.EntityQuery
                    .from(resourceName)
                    .expand(predicate);

                return executeQuery(query);
            };


            this.expandLocally = function (predicate) {
                var query = breeze.EntityQuery
                    .from(resourceName)
                    .expand(predicate);

                return executeCacheQuery(query);
            };

            this.expandWithFind = function (value, predicate) {
                var query = breeze.EntityQuery
                    .from(resourceName)
                    .expand(value)
                    .where(predicate);

                return executeQuery(query);
            };

            this.findInCache = function (predicate) {
                var query = breeze.EntityQuery
                    .from(resourceName)
                    .where(predicate);

                return executeCacheQuery(query);
            };

            this.all = function () {
                var query = breeze.EntityQuery
                    .from(resourceName);

                return executeQuery(query);
            };

            this.createEntityInCache = function (Jsonvalue) {
                var type = getMetastore().getEntityType(entityTypeName);
                var newtype = type.createEntity(Jsonvalue);
                manager().addEntity(newtype);
            };

            this.createEntity = function (Jsonvalue) {
                return manager().createEntity(entityTypeName, Jsonvalue);
            };

            this.createImageEntity = function (Jsonvalue) {
                return manager().createEntity(entityTypeName, Jsonvalue);
            }

            this.createEmptyEntity = function () {
                return manager().createEntity(entityTypeName);
            };

            this.addEntity = function (entity) {
                return manager().addEntity(entity);
            };

            this.getEntity = function () {
                return manager().getEntities(entityTypeName);
            };

            this.getEntityInCache = function () {
                var entityType = getMetastore().getEntityType(entityTypeName);
                return manager().getEntities(entityType);
            };

            this.withParameters = function (predicate) {
                var query = breeze.EntityQuery
                    .from(resourceName)
                    .withParameters(predicate);

                return executeQuery(query);
            };

            this.CustomWidgetQueryForInventory = function (value, predicate, takeVal, orderByVal) {
                var query = breeze.EntityQuery
                    .from(resourceName)
                    .expand(value)
                    .where(predicate)
                    .orderByDesc(orderByVal)
                    .take(takeVal);

                return executeQuery(query);
            }

            this.CustomWidgetQueryForInspection = function (value, predicate, takeVal, orderByVal) {
                var query = breeze.EntityQuery
                    .from(resourceName)
                    .expand(value)
                    .where(predicate)
                    .orderByDesc(orderByVal)
                    .take(takeVal);

                return executeQuery(query);
            }

            function executeQuery(query) {
                return entityManagerProvider.manager()
                    .executeQuery(query.using(fetchStrategy || breeze.FetchStrategy.FromServer))
                    .then(function (data) {
                        return data.results;
                    });
            }

            function executeCacheQuery(query) {
                return entityManagerProvider.manager().executeQueryLocally(query);
            }

            function getMetastore() {
                return manager().metadataStore;
            }

            function manager() {
                return entityManagerProvider.manager();
            }
        };

        return repository;
    })();

    return {
        create: create
    };

    function create(entityManagerProvider, entityTypeName, resourceName, fetchStrategy) {
        return new Repository(entityManagerProvider, entityTypeName, resourceName, fetchStrategy);
    }
}]);