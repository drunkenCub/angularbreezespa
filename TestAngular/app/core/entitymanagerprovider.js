﻿TestAngular.factory('entitymanagerprovider', ['breeze', 'Q',
    function (breeze, Q) {

        var apiBaseUrl = 'http://localhost:9001/'; //local
        breeze.NamingConvention.none.setAsDefault();
        var serviceName = apiBaseUrl + 'breeze/spa';
        var masterManager = new breeze.EntityManager(serviceName);

        var readyDeferred = Q.defer();
        whenReady = readyDeferred.promise;
        var EntityManagerProvider = (function () {

            var entityManagerProvider = function () {
                var manager;

                this.manager = function () {
                    if (!manager) {
                        manager = masterManager.createEmptyCopy();

                        // Populate with lookup data
                        manager.importEntities(masterManager.exportEntities());

                        // Subscribe to events
                        manager.hasChangesChanged.subscribe(function (args) {
                            //alert("haschanged");
                        });
                    }
                    return manager;
                };
            };

            return entityManagerProvider;
        })();

        var datacontext = {
            prepare: prepare,
            create: create,
            whenReady: whenReady
        };

        initializeDatacontext();

        return datacontext;

        function create() {
            return new EntityManagerProvider();
        }

        function prepare() {
            //return masterManager.fetchMetadata()
            //    .then(function () {
            //        var predicate = { orgid: 1 };
            //        var query = breeze.EntityQuery
            //            .from('lookups').withParameters(predicate);
            //        return masterManager.executeQuery(query).then(function (data) {
            //            console.log(data);
            //            deferred.resolve(data);
            //        });

            //    });
            //return deferred.promise;
            //var deferred = $q.defer();

            //var deferred = $q.defer();
            //deferred.promise.then(function () {
            //    masterManager.fetchMetadata()
            //        .then(function () {
            //            var predicate = { orgid: 1 };
            //            var query = breeze.EntityQuery
            //                .from('lookups').withParameters(predicate);
            //            masterManager.executeQuery(query).then(function () {
            //                return data;
            //            });;
            //        });
            //});
            //deferred.resolve();

        }
        function initializeDatacontext() {
            masterManager.fetchMetadata()
                   .then(function () {
                       var predicate = { orgid: 1 };
                       var query = breeze.EntityQuery
                           .from('lookups').withParameters(predicate);
                       masterManager.executeQuery(query).then(function (data) {
                           readyDeferred.resolve();
                           return data.results[0];
                       });
                       // do success stuff;
                   })
                   .fail(function (error) {
                       readyDeferred.reject(error);
                       // do error stuff;
                   });
        }
    }]);