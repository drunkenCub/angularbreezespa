﻿TestAngular.factory('unitofwork',['entitymanagerprovider', 'repository',
    function (entityManagerProvider, repository) {

        var refs = {};
        var instance = null;

        var UnitOfWork = (function () {
            var unitofwork = function () {
                var provider = entityManagerProvider.create();
                this.hasChanges = function () {
                    return provider.manager().hasChanges();
                };

                this.commit = function () {

                    var deferred = Q.defer();

                    var saveOptions = new breeze.SaveOptions({ resourceName: 'savechanges' });

                    provider.manager().saveChanges(null, saveOptions)
                        .then(function (saveResult) {
                            //app.trigger('saved', saveResult.entities);
                            deferred.resolve(saveResult);
                        });
                    return deferred.promise;
                };

                this.rollback = function () {
                    provider.manager().rejectChanges();
                };

                this.cars = repository.create(provider, 'Car', 'cars');
                this.carmodels = repository.create(provider, 'CarModel', 'carmodels', breeze.FetchStrategy.FromLocalCache);
                this.carmakes = repository.create(provider, 'CarMake', 'carmakes', breeze.FetchStrategy.FromLocalCache);
            };

            return unitofwork;
        })();

        var SmartReference = (function () {

            var ctor = function () {
                var value = null;

                this.referenceCount = 0;

                this.value = function () {
                    if (value === null) {
                        value = new UnitOfWork();
                    }

                    this.referenceCount++;
                    return value;
                };

                this.clear = function () {
                    value = null;
                    this.referenceCount = 0;

                    clean();
                };
            };

            ctor.prototype.release = function () {
                this.referenceCount--;
                if (this.referenceCount === 0) {
                    this.clear();
                }
            };

            return ctor;
        })();

        return {
            create: create,
            get: get
        };

        function create() {
            if (instance != null) {
                return instance;
            }
            else {
                instance = new UnitOfWork();
                return instance;
            }
        }

        function get(key) {
            if (!refs[key]) {
                refs[key] = new SmartReference();
            }

            return refs[key];
        }

        function clean() {
            for (key in refs) {
                if (refs[key].referenceCount == 0) {
                    delete refs[key];
                }
            }
        }
    }]);