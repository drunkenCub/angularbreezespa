﻿TestAngular.controller('InventoryCtrl', ['$scope', 'unitofwork', 'breeze', '$q', 'entitymanagerprovider', function ($scope, unitofwork, breeze, $q, entityManagerProvider) {
    entityManagerProvider.whenReady.then(function () {
        $scope.cars = [];
        var uow = unitofwork.create();
        var predicate = breeze.Predicate.create("Id", "==", 101);
        $scope.welcomeMessage = "Hello world";
        $scope.Makes = uow.carmakes.getEntityInCache();
        $scope.Models = uow.carmodels.getEntityInCache();
        getCars().then(function () {

        });
        console.log($scope.Models);

        function getCars() {
            var deferred = $q.defer();
            uow.cars.expand("Make", "Model").then(function (data) {
                $scope.cars = data;
                deferred.resolve(true);
            });
            return deferred.promise;
        }

        //$scope.Makes = test;

        $scope.saveDetails = function () {
            uow.commit().then(function () {

            });;
        }
    });
}]);